package com.ont.donormicroservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="donors")
public class donor {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name="fecha_donacion")
    private Date donationDate;
    @Column(name="codigo_hospital_donante")
    private long donorHospitalCode;
    @Column(name = "hospital_donante")
    private String donorHospital;
    @Column(name = "fecha_nacimiento")
    private Date dateOfBirth;
    @Column(name = "sexo")
    private String sex;
    @Column(name = "grupo_sanguineo")
    private String bloodType;
    @Column(name = "causa_muerte")
    private String causesDeath;
    @Column(name="pais_nacimiento")
    private String countryOfBirth;
    @Column(name = "comunidad_autonoma_residencia")
    private String autonomousCommunityResidence;
    @Column(name = "tipo_drne")
    private String drneType;
    @Column(name = "tipo_vigilancia")
    private String surveillanceType;
    @Column(name = "codigo_vigilancia")
    private String surveillanceCode;
    @Column(name = "seguimiento_receptores")
    private String trackingReceivers;
    @Column(name = "tipo_alerta")
    private String alertType;
    @Column(name = "estado_drne")
    private String drneStatus;

    public donor() {
    }

    public donor(long id, Date donationDate, long donorHospitalCode, String donorHospital, Date dateOfBirth, String sex, String bloodType, String causesDeath, String countryOfBirth, String autonomousCommunityResidence, String drneType, String surveillanceType, String surveillanceCode, String trackingReceivers, String alertType, String drneStatus) {
        this.id = id;
        this.donationDate = donationDate;
        this.donorHospitalCode = donorHospitalCode;
        this.donorHospital = donorHospital;
        this.dateOfBirth = dateOfBirth;
        this.sex = sex;
        this.bloodType = bloodType;
        this.causesDeath = causesDeath;
        this.countryOfBirth = countryOfBirth;
        this.autonomousCommunityResidence = autonomousCommunityResidence;
        this.drneType = drneType;
        this.surveillanceType = surveillanceType;
        this.surveillanceCode = surveillanceCode;
        this.trackingReceivers = trackingReceivers;
        this.alertType = alertType;
        this.drneStatus = drneStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDonationDate() {
        return donationDate;
    }

    public void setDonationDate(Date donationDate) {
        this.donationDate = donationDate;
    }

    public long getDonorHospitalCode() {
        return donorHospitalCode;
    }

    public void setDonorHospitalCode(long donorHospitalCode) {
        this.donorHospitalCode = donorHospitalCode;
    }

    public String getDonorHospital() {
        return donorHospital;
    }

    public void setDonorHospital(String donorHospital) {
        this.donorHospital = donorHospital;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getCausesDeath() {
        return causesDeath;
    }

    public void setCausesDeath(String causesDeath) {
        this.causesDeath = causesDeath;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public String getAutonomousCommunityResidence() {
        return autonomousCommunityResidence;
    }

    public void setAutonomousCommunityResidence(String autonomousCommunityResidence) {
        this.autonomousCommunityResidence = autonomousCommunityResidence;
    }

    public String getDrneType() {
        return drneType;
    }

    public void setDrneType(String drneType) {
        this.drneType = drneType;
    }

    public String getSurveillanceType() {
        return surveillanceType;
    }

    public void setSurveillanceType(String surveillanceType) {
        this.surveillanceType = surveillanceType;
    }

    public String getSurveillanceCode() {
        return surveillanceCode;
    }

    public void setSurveillanceCode(String surveillanceCode) {
        this.surveillanceCode = surveillanceCode;
    }

    public String getTrackingReceivers() {
        return trackingReceivers;
    }

    public void setTrackingReceivers(String trackingReceivers) {
        this.trackingReceivers = trackingReceivers;
    }

    public String getAlertType() {
        return alertType;
    }

    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }

    public String getDrneStatus() {
        return drneStatus;
    }

    public void setDrneStatus(String drneStatus) {
        this.drneStatus = drneStatus;
    }

    @Override
    public String toString() {
        return "donor{" +
                "id=" + id +
                ", donationDate=" + donationDate +
                ", donorHospitalCode=" + donorHospitalCode +
                ", donorHospital='" + donorHospital + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", sex='" + sex + '\'' +
                ", bloodType='" + bloodType + '\'' +
                ", causesDeath='" + causesDeath + '\'' +
                ", countryOfBirth='" + countryOfBirth + '\'' +
                ", autonomousCommunityResidence='" + autonomousCommunityResidence + '\'' +
                ", drneType='" + drneType + '\'' +
                ", surveillanceType='" + surveillanceType + '\'' +
                ", surveillanceCode='" + surveillanceCode + '\'' +
                ", trackingReceivers='" + trackingReceivers + '\'' +
                ", alertType='" + alertType + '\'' +
                ", drneStatus='" + drneStatus + '\'' +
                '}';
    }
}
