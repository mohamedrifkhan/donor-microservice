package com.ont.donormicroservice.service.impl;

import com.ont.donormicroservice.model.donor;
import com.ont.donormicroservice.repo.donorRepo;
import com.ont.donormicroservice.service.donorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class donorServiceImpl implements donorService {

    @Autowired
    private donorRepo donorRepo;

    @Override
    public List<donor> getAllDonors() {
        List<donor> donorList= donorRepo.findAll();
        return donorList;
    }
}
