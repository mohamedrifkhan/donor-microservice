package com.ont.donormicroservice.service;


import com.ont.donormicroservice.model.donor;

import java.util.List;

public interface donorService {
    List<donor> getAllDonors();
}
