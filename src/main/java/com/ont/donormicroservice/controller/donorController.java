package com.ont.donormicroservice.controller;

import com.ont.donormicroservice.model.donor;
import com.ont.donormicroservice.service.donorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/donor/*")
public class donorController {
    @Autowired
    private donorService donorService;

    @GetMapping(value = "getAll")
    List<donor> getAllDonors(){
        List<donor> donorList=donorService.getAllDonors();
        return donorList;
    }

    @GetMapping(value = "get")
    String get(){
        return "Hello";
    }
}
