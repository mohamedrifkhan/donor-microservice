package com.ont.donormicroservice.repo;


import com.ont.donormicroservice.model.donor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface donorRepo extends JpaRepository<donor,Long> {
}
